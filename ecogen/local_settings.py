# Settings for devel server
import os
from os import path
DEBUG = True
TEMPLATE_DEBUG = DEBUG
#ROOT_PATH = os.path.join(os.path.dirname(__file__), '..')
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'USER': 'ecogenusr',
        'PASSWORD': 'piano123',
        'HOST': 'localhost',
        'PORT': '3306',
        'NAME': 'ecogenesis_snap',
    },


}

#STATIC_ROOT = path.join(ROOT_PATH, 'static').replace('\\', '/')
#STATIC_ROOT = '/var/djangoprojects/caeproy/caeproy/static/'

#MEDIA_ROOT = os.path.join(ROOT_PATH, 'media')
#MEDIA_URL = 'http://127.0.0.1:8000/media/'
MEDIA_URL = '/site_media/'
#ADMIN_MEDIA_PREFIX = '/media/admin/'
TEMPLATE_DIRS = (
    # Put strings here, like "/home/html/django_templates" or "C:/www/django/templates".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
    #os.path.join(ROOT_PATH, 'templates'),
    "/var/djangoprojects/caeproy/caeproy/templates",
)


#define your backend authentification

'''
AUTH_PROFILE_MODULE = 'gestion.perfil'

AUTHENTICATION_BACKENDS = (
    'gestion.ldapbe.LDAPBackend',
    'django.contrib.auth.backends.ModelBackend',
)


##LDAP CONFIG##########################
AD_DNS_NAME = '172.16.0.13'
AD_DNS_NAME_POWER_CAMPUS = '172.16.0.16'
AD_LDAP_PORT = 389

AD_SEARCH_DN = 'ou=people, dc=unach, dc=cl'

AD_SEARCH_DN_ALU = 'ou=people, dc=alu, dc=unach, dc=cl'

AD_SEARCH_DN_POWER_CAMPUS = 'ou=PowerCampus, dc=unach2, dc=cl'

AD_SEARCH_FIELDS = ['mail','givenName','sn']

AD_LDAP_URL = 'ldap://%s:%s' % (AD_DNS_NAME,AD_LDAP_PORT)
AD_LDAP_URL_POWER_CAMPUS = 'ldap://%s:%s' % (AD_DNS_NAME_POWER_CAMPUS,AD_LDAP_PORT)
#####################################################################################
'''